# go-bits

Go-bits is a Go library implementing a 2D bit matrix.  Logically it is similar to a 2-dimensional array of booleans, but has some significant differences:

1. It is optimised for operating on blocks of data in the matrix, including:
    1. Batch copies from one matrix to another
    2. Batch bitwise operations (And, Not, Or, Xor)
2. It provides some simple structures to facilitate reading along columns and rows.
3. Unlike a `[][]bool`, A `bits.Matrix` cannot represent a jagged array.

## Context

Although the library itself is agnostic of the use case, it was written specifically to act as a primitive underpinning a solution for modelling and driving dot-matrix displays.

As such, the features implemented are likely to be only those ones that are useful in that use case.

## Usage

Import from the following path.

```golang
import "gitlab.com/realency/go-bits/pkg/bits"
```

All public types and functions are documented in the usual Godoc fashion, and usage should be fairly intuitive.

The following code snippet should serve as a useful getting-started primer.

```golang
	m1 := bits.NewMatrix(8, 32) // Create a new Matrix using NewMatrix
	m1.Put(2, 2, true)          // Put assigns state to a bit in the Matrix
	_ = m1.Get(2, 2)            // Get reads a bit in the Matrix
	m2 := m1.Clone()            // Clone creates a new copy of the matrix
	m2.Not()                    // Not inverts all the bits in the matrix
	m2.Xor(m1)                  // Logically m2[i,j] = m2[i,j] xor m1[i,j], for all i and j
```

### Cursors

Cursors provide a mechanism for linear reads across, up, or down a `Matrix`.  Again usage is illustrated by way of example.

```golang
	c := bits.NewCursor(m1, 3, 8) // Create a cursor on the target matrix and at a given offset
	_, ok := c.ReadRight()        // Read a bit and position the cursor one to the right
	if !ok {                      // the ok return value indicates if the bit was successfully read
		panic("Just read past the end of the Matrix")
	}

	_, _ = c.ReadLeft() // Other methods similarly implemented
	_, _ = c.ReadUp()
	_, _ = c.ReadDown()

	_, i := c.ReadRightByte() // ReadRightByte reads a byte from 8 consecutive rightward reads
	if i < 8 {                // The bits return value indicates the number of bits successfully read
		panic("Did not get a whole byte owing to hitting the end of the Matrix")
	}
```

## Licence

This code is freely available to anyone who stumbles upon it.  It is part of a personal project and, as such, comes with no guarantees of future support.  You may do with it as you please, so long as you don't try to pass my work off as your own, or anybody else's.

© Nicklas Chapman 2023