package bits

import (
	"strings"
)

// encoding version used to ensure that decoder is using the same format as the encoder
const encodingVersion uint32 = 1

// A Matrix is a two-dimensional array of bits with fixed height and width.
//
// Matrix is not thread-safe.  Read-only operations such as Get and Clone
// may safely be called concurrently.  Write operations such as Set and Clear
// should not be called concurrently with each other, or with read operations.
type Matrix struct {
	state         []uint32 // An array of unsigned ints as a bitfield storing the individual bits of the matrix, as well as some metadata
	bits          []uint32 // A subrange of the state slice representing just the bits data
	height, width int
	intsPerRow    int // Number of elements of the bits slice per row of the matrix, calculated when the Matrix is initialised and stored for reuse
}

// ZeroMatrix is zero-sized matrix
var ZeroMatrix *Matrix = &Matrix{}

// NewMatrix creates a new Matrix with a given size
func NewMatrix(height, width int) *Matrix {
	if height < 0 || width < 0 {
		panic("Arg out of bounds")
	}

	if height == 0 || width == 0 {
		return ZeroMatrix
	}

	var rowLen int = calcRowLen(width)
	var datalen = (rowLen * height) + 1
	state := make([]uint32, datalen+3) // 3 additional records used to store encoding version, height and width
	result := &Matrix{
		state:      state,
		bits:       state[0:datalen],
		height:     height,
		width:      width,
		intsPerRow: rowLen,
	}

	// Additional metadata added to the end of the data records
	// to simplify encoding and decoding.  Everything needed is in the array
	result.state[datalen] = encodingVersion
	result.state[datalen+1] = uint32(height)
	result.state[datalen+2] = uint32(width)

	return result
}

// FromBools builds a new Matrix from a slice of bool slices.  Input must not be jagged, or the function will panic.
func FromBools(source [][]bool) *Matrix {
	h := len(source)
	if h == 0 {
		return ZeroMatrix
	}

	w := len(source[0])
	if w == 0 {
		return ZeroMatrix
	}

	result := NewMatrix(h, w)
	for i, row := range source {
		for j, v := range row {
			result.Put(i, j, v)
		}
	}

	return result
}

// Size returns the size of the Matrix as a two-tuple of height and width
func (m *Matrix) Size() (height, width int) {
	return m.height, m.width
}

// Get returns the state of a specific bit in the Matrix.
// Arguments specify the coordinates of the bit.  Get will panic if
// the arguments are out of bounds.
func (m *Matrix) Get(row, col int) bool {
	i, k := m.selector(row, col)
	return (m.bits[i] & k) != 0
}

// Set allocates a value of true to a specific bit in the Matrix.
// Arguments specify the coordinates of the bit.
// Set will panic if the arguments are out of bounds.
func (m *Matrix) Set(row, col int) {
	i, k := m.selector(row, col)
	m.bits[i] |= k
}

// Reset allocates a value of false to a specific bit in the Matrix.
// Arguments specify the coordinates of the bit.
// Reset will panic if the arguments are out of bounds.
func (m *Matrix) Reset(row, col int) {
	i, k := m.selector(row, col)
	m.bits[i] &= (k ^ 0xFFFFFFFF)
}

// Put allocates state to a specific bit in the Matrix.
// Arguments specify the coordinates of the bit and the required state.
// Put will panic if the arguments are out of bounds.
func (m *Matrix) Put(row, col int, value bool) {
	if value {
		m.Set(row, col)
	} else {
		m.Reset(row, col)
	}
}

// Clear resets all the bits in the matrix back to zero.
func (m *Matrix) Clear() {
	copy(m.bits, make([]uint32, len(m.bits)))
}

// Clone creates an exact copy of the Matrix in its current state.
func (m *Matrix) Clone() *Matrix {
	state := make([]uint32, len(m.state))
	result := &Matrix{
		state:      state,
		bits:       state[:len(m.state)-3],
		height:     m.height,
		width:      m.width,
		intsPerRow: m.intsPerRow,
	}
	copy(result.state, m.state)
	return result
}

// Copy copies a sub-range of bits from one matrix to another.  All the bits in the range are overwritten
// in the destination matrix.
//
// Copies from source matrix, at origin (sourceRow, sourceCol) to the dest matrix ar (destRow, destCol).
// Copy will panic if either the source origin or the destination origin are out of bounds.
// Copies a rectangle up to the size given by height and width.  If the maximum-sized rectangle exceeds
// the bonds of either the source or destination matrix, it is trimmed.
// Returns the actual height and width of the rectangle copied as a result.
//
// Copying is a read-only operation with respect to the source matrix, with the usual implications for
// concurrency.
func Copy(source *Matrix, sourceRow, sourceCol int, dest *Matrix, destRow, destCol, height, width int) (int, int) {
	// A whole load of bounds-checking and trimming logic
	if height == 0 || width == 0 {
		return 0, 0
	}
	if height < 0 || width < 0 {
		panic("Arg out of bounds")
	}
	if sourceRow < 0 || sourceRow >= source.height || sourceCol < 0 || sourceCol >= source.width {
		panic("Arg out of bounds")
	}
	if destRow < 0 || destRow >= dest.height || destCol < 0 || destCol >= dest.width {
		panic("Arg out of bounds")
	}
	if height > source.height-sourceRow {
		height = source.height - sourceRow
	}
	if height > dest.height-destRow {
		height = dest.height - destRow
	}
	if width > source.width-sourceCol {
		width = source.width - sourceCol
	}
	if width > dest.width-destCol {
		width = dest.width - destCol
	}

	// The actual copy operation is performed using stream readers on the source and writers on the destination
	for i := 0; i < height; i++ {
		r := newStream(source, sourceRow+i, sourceCol, width)
		w := newStream(dest, destRow+i, destCol, width)
		streamCopy(r, w)
	}

	return height, width
}

// Encode returns the complete state of the matrix as an array of UInt32.
//
// Provided as a means to serialise a matrix for persistance or transport.
func (m *Matrix) Encode() []uint32 {
	result := make([]uint32, len(m.state))
	copy(result, m.state)
	return result
}

// Decode creates a new Matrix from an array of UInt32.
//
// Provided as a complement to the Encode method, builds a Matrix from the state
// encoded by that method.
func Decode(source []uint32) *Matrix {
	m := len(source) - 3
	if source[m] != encodingVersion {
		panic("unrecognised encoding version decoding array to bits.Matrix")
	}

	result := &Matrix{
		state:  make([]uint32, len(source)),
		height: int(source[m+1]),
		width:  int(source[m+2]),
	}
	copy(result.state, source)
	result.intsPerRow = calcRowLen(result.width)

	return result
}

// String generates a string representation of the matrix.
// The string generated is intended for visual inspection, and applies
// a style appropriate to that.
func (m *Matrix) String() string {
	var sb strings.Builder
	for i := 0; i < m.height; i++ {
		for j := 0; j < m.width; j++ {
			if m.Get(i, j) {
				sb.WriteString("@ ")
			} else {
				sb.WriteString(". ")
			}
		}
		sb.WriteRune('\n')
	}
	return sb.String()

}

// A utility function to find the index into the m.bits array and the appropriate
// Bitwise mask to select the bit, given a bit's coordinates.
// Panics if the arguments are out of range.
func (m *Matrix) selector(row, col int) (index int, mask uint32) {
	if row < 0 || row >= m.height || col < 0 || col >= m.width {
		panic("Arg out fo range")
	}
	return (m.intsPerRow * row) + (col / 32), uint32(0x80000000) >> (col % 32)
}

// Calculates the number of whole integers required to contain a single row of the matrix
func calcRowLen(width int) int {
	return ((width - 1) >> 5) + 1
}
