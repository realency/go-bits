package bits

import (
	"bufio"
	"errors"
	"io"
)

var ErrInvalidMatrixFormat = errors.New("invalid serialisation format for bits.Matrix")

type BitMatrixReader interface {
	Read(reader io.Reader) (*Matrix, error)
}

type BitMatrixWriter interface {
	Write(writer io.Writer, matrix *Matrix) error
}

type BitMatrixReaderWriter interface {
	BitMatrixReader
	BitMatrixWriter
}

var def BitMatrixReaderWriter = HumanReadable{
	TrueRune:   '@',
	FalseRune:  '.',
	NewRowRune: '\n',
	Terminator: '-',
	Padding:    1,
}

func DefaultReaderWriter() BitMatrixReaderWriter {
	return def
}

func Read(reader io.Reader) (*Matrix, error) {
	return DefaultReaderWriter().Read(reader)
}

func Write(writer io.Writer, matrix *Matrix) error {
	return DefaultReaderWriter().Write(writer, matrix)
}

type HumanReadable struct {
	TrueRune   rune
	FalseRune  rune
	NewRowRune rune
	Terminator rune
	Padding    int
}

func (rw HumanReadable) Read(reader io.Reader) (result *Matrix, e error) {
	var bools [][]bool
	var c rune

	r := bufio.NewReader(reader)
	row := []bool{}
	for c, _, e = r.ReadRune(); e == nil && c != rw.Terminator; c, _, e = r.ReadRune() {
		switch c {
		case ' ':
			continue
		case rw.TrueRune:
			row = append(row, true)
		case rw.FalseRune:
			row = append(row, false)
		case rw.NewRowRune:
			bools = append(bools, row)
			row = []bool{}
		default:
			return nil, ErrInvalidMatrixFormat
		}
	}
	if e != nil {
		return nil, e
	}

	defer func() {
		if p := recover(); p != nil {
			e = ErrInvalidMatrixFormat
		}
	}()

	return FromBools(bools), nil
}

func (rw HumanReadable) Write(writer io.Writer, matrix *Matrix) error {
	w := bufio.NewWriter(writer)

	writeValue := func(c rune) error {
		for i := 0; i < rw.Padding; i++ {
			if _, e := w.WriteRune(' '); e != nil {
				return e
			}
		}
		_, e := w.WriteRune(c)
		return e
	}

	for i := 0; i < matrix.height; i++ {
		for j := 0; j < matrix.width; j++ {
			if matrix.Get(i, j) {
				if e := writeValue(rw.TrueRune); e != nil {
					return e
				}
			} else {
				if e := writeValue(rw.FalseRune); e != nil {
					return e
				}
			}
		}
		if _, e := w.WriteRune(rw.NewRowRune); e != nil {
			return e
		}
	}
	_, e := w.WriteRune(rw.Terminator)
	w.Flush()
	return e
}
