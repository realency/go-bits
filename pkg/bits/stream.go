package bits

// Stream allows reading or writing across a single row, withing a range
// Used internally for copying ranges from one matrix to another and is
// NOT intended for any external purpose such as serialisation
type stream struct {
	buff      []uint32 // the underlying data from the matrix
	index     int      // index into the buff of the integer currently being read
	offset    int      // offset into the current uint32, as number of bits
	available int      // total number of bits left available in the range being read from/written to.  Bounded by an argument when the stream is created
}

func newStream(m *Matrix, row, col, count int) *stream {
	if row < 0 || row >= m.height || col < 0 || col >= m.width || col+count > m.width {
		panic("Argument out of bounds")
	}

	return &stream{
		buff:      m.bits,
		index:     (m.intsPerRow * row) + (col / 32),
		offset:    col % 32,
		available: count,
	}
}

// Returns the number of bits readable from the current UInt32 based on:
// * the number of bits wanted (count)
// * the number of bits left in the uint32 currently being read
// * the bound placed on stream when it was created.
//
// IMPORTANT NOTE: This is NOT the number of bits left to read/write, rather
// just the number let in the current UInt32.  The full range is read/written
// via repeated calls.
func (r *stream) usable(count int) int {
	n := count
	if n > 32 {
		n = 32
	}
	if r.available < n {
		n = r.available
	}
	if (32 - r.offset) < n {
		n = 32 - r.offset
	}
	return n
}

// seeks forward through the range by count bits.
// Callers guarantee not to advance beyond the end of the
// current uint32
func (r *stream) seek(count int) {
	r.available -= count
	r.offset += count
	if r.offset == 32 {
		r.offset = 0
		r.index++
	}
}

// reads a block of bits from the row
// * data read is written into dest
// * reads no more than count bits, but may read fewer
// * returns number of bits actually read
// * reading fewer bits than requested does NOT indicate reaching the end of the range
// * returns 0 when the end is met
func (r *stream) read(dest *uint32, count int) int {
	count = r.usable(count)

	switch count {
	case 0:
		return 0
	case 32:
		// Optimisation for common case where we're reading the whole int.
		*dest = r.buff[r.index]
		r.index++
		r.available -= 32
		return 32
	default:
		*dest = r.buff[r.index] << r.offset
		r.seek(count)
		return count
	}
}

// writes a block of bits to the row
// * bits for writing in source
// * writes no more than count bits, but may write fewer
// * returns number of bits actually written
// * writing fewer bits than requested does NOT indicate reaching the end of the range
// * returns 0 when the end is met
func (r *stream) write(source uint32, count int) int {
	count = r.usable(count)
	switch count {
	case 0:
		return 0
	case 32:
		// Optimisation for the common case where we're writing the whole int
		r.buff[r.index] = source
		r.available -= 32
		r.index++
		return 32
	default:
		var mask uint32 = (0xFFFFFFFF << (32 - count)) >> r.offset
		source = (source >> r.offset) & mask
		mask ^= 0xFFFFFFFF
		mask &= r.buff[r.index]
		r.buff[r.index] = mask | source

		r.seek(count)
		return count
	}
}

// Copies from one range in a row of one matrix to the range of another
// Takes a reader stream and a writer stream.
// Returns total number of bits copied.
func streamCopy(source, dest *stream) int {
	var buff uint32
	count := 0
	for {
		var readCount int
		if readCount = source.read(&buff, 32); readCount == 0 {
			return count
		}

		for readCount > 0 {
			var writeCount int
			if writeCount = dest.write(buff, readCount); writeCount == 0 {
				return count
			}
			count += writeCount
			readCount -= writeCount
		}
	}
}
