// Package bits provides a 2D bit matrix.  Logically it is similar to a 2-dimensional array of booleans, but has some significant differences:
//  1. It is optimised for operating on blocks of data in the matrix, including:
//     a. Batch copies from one matrix to another
//     b. Batch bitwise operations (And, Not, Or, Xor)
//  2. It provides some simple structures to facilitate reading along columns and rows.
//  3. Unlike a [][]bool, A bits.Matrix cannot represent a jagged array.
package bits
