package main

import (
	"gitlab.com/realency/go-bits/pkg/bits"
)

func main() {
	m1 := bits.NewMatrix(8, 32) // Create a new Matrix using NewMatrix
	m1.Put(2, 2, true)          // Put assigns state to a bit in the Matrix
	_ = m1.Get(2, 2)            // Get reads a bit in the Matrix
	m2 := m1.Clone()            // Clone creates a new copy of the matrix
	m2.Not()                    // Not inverts all the bits in the matrix
	m2.Xor(m1)                  // Logically m2[i,j] = m2[i,j] xor m1[i,j], for all i and j

	c := bits.NewCursor(m1, 3, 8) // Create a cursor on the target matrix and at a given offset
	_, ok := c.ReadRight()        // Read a bit and position the cursor one to the right
	if !ok {                      // the ok return value indicates if the bit was successfully read
		panic("Just read past the end of the Matrix")
	}

	_, _ = c.ReadLeft() // Other methods similarly implemented
	_, _ = c.ReadUp()
	_, _ = c.ReadDown()

	_, i := c.ReadRightByte() // ReadRightByte reads a byte from 8 consecutive rightward reads
	if i < 8 {                // The bits return value indicates the number of bits successfully read
		panic("Did not get a whole byte owing to hitting the end of the Matrix")
	}
}
