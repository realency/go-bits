package bits_test

import (
	"bytes"
	"strings"
	"testing"

	"gitlab.com/realency/go-bits/pkg/bits"
)

func TestZeroMatrixSerialisesAsTerminator(t *testing.T) {
	target := bits.ZeroMatrix
	buff := &strings.Builder{}
	e := bits.Write(buff, target)
	if e != nil {
		t.Error(e)
	}
	if buff.String() != "-" {
		t.Errorf("Expected \"-\", but Zero Matrix serailsed as \"%s\"", buff.String())
	}
}

func TestNonZeroMatrixSerialisesToExpectedRepresentation(t *testing.T) {
	source := bits.NewMatrix(2, 3)
	source.Set(0, 0)
	source.Set(1, 1)
	source.Set(0, 2)
	buff := &strings.Builder{}
	e := bits.Write(buff, source)
	if e != nil {
		t.Error(e)
	}

	if buff.String() != " @ . @\n . @ .\n-" {
		t.Errorf("Serialised matrix did not have expected form")
	}
}

func TestNonZeroMatrixSerialisesAndDeserialisesToEqualMatrix(t *testing.T) {
	source := bits.NewMatrix(10, 10)
	source.Set(2, 3)
	source.Set(9, 9)
	source.Set(4, 0)

	buff := bytes.NewBuffer(nil)
	e := bits.Write(buff, source)
	if e != nil {
		t.Error(e)
	}

	target, e := bits.Read(bytes.NewBuffer(buff.Bytes()))
	if e != nil {
		t.Error(e)
	}

	if !source.Equals(target) {
		t.Error("Returned Matrix was not equivalent to the source")
	}
}

func TestTerminatorDeserialisesAsZeroMatrix(t *testing.T) {
	buff := bytes.NewBufferString("-")
	m, e := bits.Read(buff)
	if e != nil {
		t.Error(e)
	}

	if m != bits.ZeroMatrix {
		t.Error("Expected zero matrix, but got another")
	}
}
